from dataclasses import dataclass

import pytest

from sync.backends.inmemory import InMemoryLinkProvider, InMemoryDs
from demo.daphne import VlanServiceConnection
from demo.desire import NIC, CloudRegion
from sync.datasource import DataSource
from sync.mapping import Mapping
from sync.syncer import Syncer, NotLinked


def test_resolve_values():
    @dataclass(frozen=True)
    class Child:
        parent: object = None
        key: str = None

    @dataclass(frozen=True)
    class Parent:
        key: str = None
        parent: object = None

    @dataclass(frozen=True)
    class GrandParent:
        key: str = None

    @dataclass(frozen=True)
    class BGrandParent:
        pass

    @dataclass(frozen=True)
    class BParent:
        pass

    entity = Child(
        key="value",
        parent=Parent(),
    )
    b_parent = BParent()

    link_provider = InMemoryLinkProvider()

    syncer = Syncer(
        InMemoryDs.from_collection(set()),
        InMemoryDs.from_collection(set()),
        [Mapping()],
        link_provider=link_provider,
    )

    link_provider.link(entity.parent, b_parent, None, None)

    assert syncer._resolve_values(entity, "key") == ("value", None)
    result = syncer._resolve_values(entity, "parent", Parent, BParent)
    assert result == (entity.parent, b_parent)

    link_provider = InMemoryLinkProvider()
    syncer.link_provider = link_provider
    with pytest.raises(NotLinked):
        syncer._resolve_values(entity, "parent", Parent, BParent)

    b_grand_parent = BGrandParent()
    entity = Child(
        parent=Parent(
            key="value",
            parent=(grand_parent := GrandParent(key="value2")),
        )
    )

    link_provider.link(grand_parent, b_grand_parent, None, None)

    assert syncer._resolve_values(entity, ("parent", "key")) == ("value", None)

    assert syncer._resolve_values(entity, ("parent", "parent", "key")) == (
        "value2",
        None,
    )

    assert grand_parent, b_grand_parent == syncer._resolve_values(
        entity, ("parent", "parent"), GrandParent, BGrandParent
    )


def test_preload_data_source(
    daphne_db: DataSource,
    desire_db: DataSource,
    daphne_desire_mappings,
    daphne_desire_mappings_deep_joins,
):
    mappings = [*daphne_desire_mappings, *daphne_desire_mappings_deep_joins]

    paths = {}

    def mock_preload_type(preload_type, path):
        if not preload_type in paths:
            paths[preload_type] = set()
        paths[preload_type].add(path)

    daphne_db.preload_type = mock_preload_type
    desire_db.preload_type = mock_preload_type

    Syncer(daphne_db, desire_db, mappings)

    assert paths.keys() == {NIC, CloudRegion, VlanServiceConnection}
    assert ("parent", "parent", "parent") in paths[NIC]
    assert ("parent",) in paths[VlanServiceConnection]
