from sync.backends.inmemory import InMemoryDs, InMemoryLinkProvider

from conftest import A, B
from sync.operations import Linking, AttributeUpdated
from sync.syncer import Syncer


def test_both_timestamped_unlinked(a_b_mappings):
    a = A(key_a="unlinked", prop_a="old")
    a.update_timestamp(1)

    b = B(key_b="unlinked", prop_b="new")
    b.update_timestamp(2)

    syncer = Syncer(
        InMemoryDs.from_collection([a]), InMemoryDs.from_collection([b]), a_b_mappings
    )
    operations = syncer.sync_all()

    assert a.prop_a == b.prop_b == "new"
    assert a.updated_at() == b.updated_at() == 2

    assert len(operations) == 2
    assert operations[0] == Linking(
        entities=frozenset({a, b}), classes=frozenset({A, B})
    )
    assert operations[1] == AttributeUpdated(
        attribute="prop_a", from_entity=b, entity=a, old_value="old", new_value="new"
    )


def test_both_timestamped_linked(a_b_mappings):
    a = A(key_a="linked old", prop_a="prop old")
    a.update_timestamp(1)

    b = B(key_b="linked new", prop_b="prop new")
    b.update_timestamp(2)

    from_ds = InMemoryDs.from_collection([a])
    to_ds = InMemoryDs.from_collection([b])
    link_provider = InMemoryLinkProvider()
    link_provider.link(a, b, from_ds, to_ds)
    syncer = Syncer(from_ds, to_ds, a_b_mappings, link_provider)
    operations = syncer.sync_all()

    assert a.prop_a == b.prop_b == "prop new"
    assert a.key_a == b.key_b == "linked new"
    assert a.updated_at() == b.updated_at() == 2

    assert len(operations) == 2
    assert operations[0] == AttributeUpdated(
        attribute="key_a",
        from_entity=b,
        entity=a,
        old_value="linked old",
        new_value="linked new",
    )

    assert operations[1] == AttributeUpdated(
        attribute="prop_a",
        from_entity=b,
        entity=a,
        old_value="prop old",
        new_value="prop new",
    )
