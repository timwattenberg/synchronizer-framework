from typing import List

import pytest
from sync.backends.inmemory import InMemoryDs, InMemoryLinkProvider

from demo.daphne import (
    VlanServiceConnection,
    VlanServiceProvider,
)
from demo.desire import (
    NIC,
    CloudServiceProvider,
)
from sync.mapping import Mapping, Mode
from sync.operations import Linking, Addition, AttributeUpdated, SkipMissingLink
from sync.syncer import MayNotUpdateJoinedAttributes, Syncer


def assert_operation_types_of_size(operations, type, size):
    len([o for o in operations if isinstance(o, type)]) == size


def test_addition_attribute_update(
    daphne_db: InMemoryDs, desire_db: InMemoryDs, daphne_desire_mappings_deep_joins
):
    link_provider = InMemoryLinkProvider()

    # Manually link CSPs to each other, so they can be used as foreign key
    manual_link(daphne_db, desire_db, link_provider)

    syncer = Syncer(
        daphne_db, desire_db, daphne_desire_mappings_deep_joins, link_provider
    )
    operations = syncer.sync_all()

    assert len(operations) == 8

    # vlan_service_connection_nic1 supposed to be found and linked
    nic_nic_1 = desire_db.find(NIC, {"name": "nic-1"})[0]
    vlan_service_connection_nic1 = daphne_db.find(
        VlanServiceConnection, {"name": "nic-1"}
    )[0]
    assert (
        Linking(
            classes=frozenset({VlanServiceConnection, NIC}),
            entities=frozenset(
                {
                    nic_nic_1,
                    vlan_service_connection_nic1,
                }
            ),
        )
        in operations
    )

    # vlan_service_connection_nic_2a supposed to be added
    nic_nic_2a = desire_db.find(NIC, {"name": "nic-2a"})[0]
    vlan_service_connection_nic_2a = daphne_db.find(
        VlanServiceConnection, {"name": "nic-2a"}
    )[0]

    assert (
        Addition(
            from_entity=nic_nic_2a,
            from_class=NIC,
            to_entity=vlan_service_connection_nic_2a,
            to_class=VlanServiceConnection,
        )
        in operations
    )

    assert (
        Linking(
            entities=frozenset({vlan_service_connection_nic_2a, nic_nic_2a}),
            classes=frozenset({VlanServiceConnection, NIC}),
        )
        in operations
    )

    nic_nic_3a = desire_db.find(NIC, {"name": "nic-3a"})[0]
    vlan_service_connection_nic3a = daphne_db.find(
        VlanServiceConnection, {"name": "nic-3a"}
    )[0]

    assert (
        Addition(
            from_class=NIC,
            to_class=VlanServiceConnection,
            from_entity=nic_nic_3a,
            to_entity=vlan_service_connection_nic3a,
        )
        in operations
    )

    nic_nic_3 = desire_db.find(NIC, {"name": "nic-3"})[0]
    vlan_service_connection_nic3 = daphne_db.find(
        VlanServiceConnection, {"externalRef": "nic3-eref"}
    )[0]

    assert (
        AttributeUpdated(
            entity=vlan_service_connection_nic3,
            from_entity=nic_nic_3,
            new_value="nic3-eref",
            old_value=None,
            attribute="externalRef",
        )
        in operations
    )

    nic_nic_4 = desire_db.find(NIC, {"name": "nic-4"})[0]
    assert (
        SkipMissingLink(
            from_entity=nic_nic_4, from_class=NIC, to_class=VlanServiceConnection
        )
        in operations
    )

    # Assume there are only the two defined additions and no more
    assert_operation_types_of_size(operations, Addition, 2)

    # Assume there is only the one defined attribute update and no more
    assert_operation_types_of_size(operations, AttributeUpdated, 1)

    # Skipping nic-4
    assert_operation_types_of_size(operations, SkipMissingLink, 1)

    # The rest is Linking
    assert_operation_types_of_size(operations, Linking, 4)


def manual_link(daphne_db, desire_db, link_provider):
    for vlsp in daphne_db.all(VlanServiceProvider):
        for csp in desire_db.find(CloudServiceProvider, {"name": vlsp.name}):
            link_provider.link(vlsp, csp, daphne_db, desire_db)


def test_not_update_deep_join(
    daphne_db: InMemoryDs,
    desire_db: InMemoryDs,
    daphne_desire_mappings_deep_joins: List[Mapping],
):
    link_provider = InMemoryLinkProvider()

    # Manually link CSPs to each other, so they can be used as foreign key
    manual_link(daphne_db, desire_db, link_provider)

    daphne_desire_mappings_deep_joins[0].modes = {Mode.LEFT_TO_RIGHT}

    syncer = Syncer(
        daphne_db, desire_db, daphne_desire_mappings_deep_joins, link_provider
    )
    with pytest.raises(MayNotUpdateJoinedAttributes):
        syncer.sync_all()
