from sync.backends.inmemory import InMemoryDs, InMemoryLinkProvider

from demo.daphne import VlanServiceProvider
from demo.desire import CloudServiceProvider
from sync.operations import AttributeUpdated
from sync.syncer import Syncer


def test_update_key(daphne_desire_mappings):
    vlan_service_provider = VlanServiceProvider(name="Linked New", canUpgrade=False)
    cloud_service_provider = CloudServiceProvider(
        name="Linked Old", upgrade_allowed=False
    )

    daphne_db = InMemoryDs.from_collection({vlan_service_provider})
    desire_db = InMemoryDs.from_collection({cloud_service_provider})

    link_provider = InMemoryLinkProvider()
    link_provider.link(
        vlan_service_provider, cloud_service_provider, daphne_db, desire_db
    )

    syncer = Syncer(daphne_db, desire_db, daphne_desire_mappings, link_provider)
    operations = syncer.sync_all()

    assert len(operations) == 1

    assert operations[0] == AttributeUpdated(
        attribute="name",
        old_value="Linked Old",
        new_value="Linked New",
        from_entity=vlan_service_provider,
        entity=cloud_service_provider,
    )


def test_update_attribute(daphne_desire_mappings):
    vlan_service_provider = VlanServiceProvider(name="Linked", canUpgrade=False)

    cloud_service_provider = CloudServiceProvider(name="Linked", upgrade_allowed=True)

    daphne_db = InMemoryDs.from_collection({vlan_service_provider})
    desire_db = InMemoryDs.from_collection({cloud_service_provider})

    link_provider = InMemoryLinkProvider()
    link_provider.link(
        vlan_service_provider, cloud_service_provider, daphne_db, desire_db
    )

    syncer = Syncer(daphne_db, desire_db, daphne_desire_mappings, link_provider)
    operations = syncer.sync_all()

    assert len(operations) == 1

    assert operations[0] == AttributeUpdated(
        attribute="upgrade_allowed",
        old_value=True,
        new_value=False,
        from_entity=vlan_service_provider,
        entity=cloud_service_provider,
    )

def test_dirty_ignore(daphne_desire_dirty_ignore):
    vlan_service_provider = VlanServiceProvider(name="Linked", canUpgrade=False)

    cloud_service_provider = CloudServiceProvider(name="Linked", upgrade_allowed=True, pending_update=True)

    daphne_db = InMemoryDs.from_collection({vlan_service_provider})
    desire_db = InMemoryDs.from_collection({cloud_service_provider})

    link_provider = InMemoryLinkProvider()
    link_provider.link(
        vlan_service_provider, cloud_service_provider, daphne_db, desire_db
    )

    syncer = Syncer(daphne_db, desire_db, [daphne_desire_dirty_ignore], link_provider)
    operations = syncer.sync_all()

    assert len(operations) == 1

    assert operations[0] == AttributeUpdated(
        attribute="upgrade_allowed",
        old_value=True,
        new_value=False,
        from_entity=vlan_service_provider,
        entity=cloud_service_provider,
    )

def test_dirty_do_not_overwirte(daphne_desire_dirty_do_not_overwrite):
    vlan_service_provider = VlanServiceProvider(name="Linked", canUpgrade=False, pending_update=True)

    cloud_service_provider = CloudServiceProvider(name="Linked", upgrade_allowed=True)

    daphne_db = InMemoryDs.from_collection({vlan_service_provider})
    desire_db = InMemoryDs.from_collection({cloud_service_provider})

    link_provider = InMemoryLinkProvider()
    link_provider.link(
        vlan_service_provider, cloud_service_provider, daphne_db, desire_db
    )

    syncer = Syncer(daphne_db, desire_db, [daphne_desire_dirty_do_not_overwrite], link_provider)
    operations = syncer.sync_all()

    assert len(operations) == 0

def test_daphne_desire_dirty_update(daphne_desire_dirty_update):
    vlan_service_provider = VlanServiceProvider(name="Linked", canUpgrade=True, pending_update=True)

    cloud_service_provider = CloudServiceProvider(name="Linked", upgrade_allowed=False)

    daphne_db = InMemoryDs.from_collection({vlan_service_provider})
    desire_db = InMemoryDs.from_collection({cloud_service_provider})

    link_provider = InMemoryLinkProvider()
    link_provider.link(
        vlan_service_provider, cloud_service_provider, daphne_db, desire_db
    )

    syncer = Syncer(daphne_db, desire_db, [daphne_desire_dirty_update], link_provider)
    operations = syncer.sync_all()

    assert len(operations) == 1

    assert operations[0] == AttributeUpdated(
        attribute="upgrade_allowed",
        old_value=False,
        new_value=True,
        from_entity=vlan_service_provider,
        entity=cloud_service_provider,
    )
