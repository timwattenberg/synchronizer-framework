FROM python:3.8-slim

RUN apt-get update ; apt-get install -y sqlite3 vim ; apt-get clean

WORKDIR /app/synchronizer-framework
COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY src/ ./src

COPY tests ./tests
COPY conftest.py .

ENV C_FORCE_ROOT=true
ENV PYTHONPATH="src/"
CMD [ "python", "-m", "demo.main"]
